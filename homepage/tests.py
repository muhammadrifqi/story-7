from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from homepage.views import homepage

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
# Create your tests here.
class UnitTestStory7(TestCase):
	def test_landingpage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'homepage.html')

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_homepage_function(self):
		response = resolve('/')
		self.assertEqual(response.func, homepage)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = homepage(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Story-7 Rifqi</title>', html_response)

class FunctionalTestStory7(TestCase):
	def setUp(self):
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome(chrome_options=chrome_options)
		super(FunctionalTestStory7, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTestStory7, self).tearDown()

	def test_accordion_displayed(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')

		self.assertIn("Aktivitas", selenium.page_source)
		self.assertIn("Organisasi-Kepanitiaan", selenium.page_source)
		self.assertIn("Prestasi", selenium.page_source)

		accordion1 = selenium.find_element_by_id('button-aktivitas')

		accordion1.click()
		self.assertIn("Staff AKPEM BEM Fasilkom UI 2019", selenium.page_source)

	def test_normal_mode_using_white_bg(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		backgroundObj = selenium.find_element_by_tag_name('body')
		self.assertEqual("rgba(255, 255, 255, 1)", backgroundObj.value_of_css_property("background-color"))